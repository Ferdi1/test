-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test`;


-- Dumping structure for table test.content
CREATE TABLE IF NOT EXISTS `content` (
  `contentID` int(11) NOT NULL AUTO_INCREMENT,
  `contentTitle` text,
  `contentDescription` text,
  `contentStatus` int(11) DEFAULT NULL,
  `contentPosition` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table test.content: ~1 rows (approximately)
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` (`contentID`, `contentTitle`, `contentDescription`, `contentStatus`, `contentPosition`) VALUES
	(1, 'hoi', 'ja', 1, NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;


-- Dumping structure for table test.images
CREATE TABLE IF NOT EXISTS `images` (
  `imageID` int(11) NOT NULL AUTO_INCREMENT,
  `imageName` varchar(255) DEFAULT NULL,
  `imageSize` int(50) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `imageStatus` int(11) DEFAULT '0',
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table test.images: ~2 rows (approximately)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`imageID`, `imageName`, `imageSize`, `imageUrl`, `imageStatus`) VALUES
	(1, 'ha', NULL, '../img/1.jpg', 1),
	(2, 'rge', NULL, '../img/2.jpg', 1);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;


-- Dumping structure for table test.users
CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL,
  `userPassword` varchar(50) NOT NULL,
  `userEmail` varchar(50) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table test.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`userID`, `userName`, `userPassword`, `userEmail`) VALUES
	(1, 'egr', 'ewf', ''),
	(2, 'dtyjydtjyjt', 'ewf', ''),
	(3, 'eee', '$2a$08$E267vFQWEM57KoAOFPp2YuwoVkl2ElLgqrX3r0wvCiR', ''),
	(4, 'aaa', '$2a$08$G7uXkhX99NbTEu9MEjoDquorMJhRZAezf5E9WKA0Kqt', ''),
	(5, 'sthtr', '$2a$08$9aW64AJeYNx9jF3rfkGedeP1MoTbGbokoALp/T6vQgL', 'drthdrth@rgse'),
	(6, 'jytjt', '$2a$08$X0P4oAHHbYKMunvVElMJKutsmfUEYb8qXWQfrX5gN3T', 'yjyjtf@dthdht.nl');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
