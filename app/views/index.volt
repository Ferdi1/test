<!DOCTYPE html>
<html>

<head>
    {% block header %}

        <title>test</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link rel="stylesheet" type="text/css" href="../../css/index.css">
        <script src="../../js/main.js"></script>

    {% endblock %}
</head>

<body>
{% block content %}

    <div class="container-fluid">

        <div class="row">

            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="../img/1.jpg" alt="Chania" height="345">
                    </div>

                    <div class="item">
                        <img src="../img/2.jpg" alt="Chania" height="345">
                    </div>

                    <div class="item">
                        <img src="../img/3.jpg" alt="Flower" height="345">
                    </div>

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div>

    </div>

{#HET CMS IN JE TEMPLATE MERGEN (met namespaces? of gewoon ernaast gezien DB?
 dan hele project als www.ditiseenwebsite.nl/admin uploaden),
 ALS ALLES PERFECT IS DAN LOGIN MODULE ERNAAST+LAYOUT LOGIN POPUP EN CMS MODULE ERNAAST DOEN !#}

    <div class="container-fluid">

        <div class="row">

            <nav class="navbar navbar-default">

                <div class="navbar-header">

                    <a class="navbar-brand" href="#">WebSiteName</a>

                </div>

                {#Navigation buttons#}
                <ul class="nav navbar-nav">
                    <li id="home"><a href="../index">Home</a></li>
                    <li><a href="../media">Media</a></li>
                    <li><a href="#">Over</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>

                {#Login button#}
                <ul class="nav navbar-nav pull-right">
                    <li><a href="users/login" data-toggle="modal" data-target="#login-modal1">Login</a></li>
                </ul>

                {#Login pop-up#}
                <div class="modal" id="login-modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    <div class="modal-dialog">

                        <div class="loginmodal-container" id="hide">

                            <h1>Login to Your Account</h1><br>

                            <form method="post" action="../users/login">
                                <input type="text" name="user" placeholder="Username" />
                                <input type="password" name="password" placeholder="Password" />
                                <input type="submit" name="" class="login loginmodal-submit" value="Login" />
                            </form>

                            <div class="login-help">
                                <a data-dismiss="modal" data-toggle="modal" data-target="#login-modal2" href="../views/users/signup.volt">Register</a> - <a href="#">Forgot Password</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="modal" id="login-modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    <div class="modal-dialog">

                        <div class="loginmodal-container">

                            <h1>Register</h1><br>

                            <form method="post" action="../users/signup">
                                <input type="text" name="user" placeholder="Username" />
                                <input type="password" name="password" placeholder="Password" />
                                <input type="email" name="email" placeholder="E-Mail" />
                                <input type="submit" name="login" class="login loginmodal-submit" value="Register" />
                            </form>

                            <div class="login-help">
                                Already have an account? <a data-dismiss="modal" data-toggle="modal" data-target="#login-modal1">Sign in</a>
                            </div>

                        </div>

                    </div>

                </div>

            </nav>

        </div>

    </div>

    <div class="container">

        <div class="row">

            {{ content() }}

        </div>

    </div>

{% endblock %}


{% block footer %}

{% endblock %}
</body>

</html>