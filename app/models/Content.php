<?php

use Phalcon\Mvc\Model;

class Content extends Model {

    public $contentID;

    public $contentTitle;

    public $contentDescription;

    public $contentStatus;

    public $contentPosition;

}