<?php

//namespace Store\Admin\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $contentDescription = Content::find(array('order'=>'contentID DESC', 'limit'=>1));

        foreach ($contentDescription as $contentID) {
            $this->view->contentDescription = $contentID->contentDescription;
        }

        $image = Images::find(array("conditions" => "imageStatus LIKE 1"));

        foreach ($image as $images) {
            $values[] = $images->imageUrl;
            $this->view->imageUrl = $values;
            $this->view->valuesLength = count($values) -1;
        }



//        $user = new Users();
//
//        $login    = $this->request->getPost("user");
//        $password = $this->request->getPost("password");
//
//        $user->userName = $login;
//
//        // Store the password hashed
//        $user->password = $this->security->hash($password);
//
//        $user->save();
    }
}