<?php

use Phalcon\Loader;
use Phalcon\Tag;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

try {

    $loader = new Loader();
    $loader->registerDirs(
        array(
            'app/controllers/',
            'app/models/',
        )
    )->register();

    $di = new FactoryDefault();

    $di['db'] = function() {
        return new DbAdapter(array(
            "host"     => "192.168.56.1",
            "username" => "mainuser",
            "password" => "dev01dev",
            "dbname"   => "test",
        ));
    };
    $di['view'] = function() {
        $view = new View();
        $view->setViewsDir('app/views/');
        $view->registerEngines(array(
            ".volt" => function($view, $di) {
                $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                $volt->setOptions(array(
                    "compiledPath" => "app/cache/volt/"
                ));
                return $volt;
            }
        ));
        return $view;
    };
    $di['url'] = function() {
        $url = new Url();
        $url->setBaseUri('');
        return $url;
    };
    $di['tag'] = function() {
        return new Tag();
    };

    $di->setShared(
        "session",
        function () {
            $session = new \Phalcon\Session\Adapter\Files();

            $session->start();

            return $session;
        }
    );

    $application = new Application($di);
    echo $application->handle()->getContent();

} catch (Exception $e) {
    echo "Exception: ", $e->getMessage();
}